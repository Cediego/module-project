/**
* @file student.h
* @author Léo B.
* @version 1.0.1
* @date 17 juin 2021
* 
*/

#ifndef STUDENT_H
#define STUDENT_H

#include <string>
#include <vector>
/**
 * Initialisation de l'objet Student 
 * pour gerer les présences
 * 
 */
class Student
{
public:
    Student(std::string name, bool present = true);

    std::string name() const;
    void setName(const std::string &name);
    bool present() const;
    /**
     * Gerer les presences
     * 
    */
    void setPresent(bool present);

    /**
     * Afficher les info 
    */
    std::string print() const;

    static int nb_presents( const std::vector<Student> &students );

private:
    std::string m_name;
    bool m_present = true;
};

#endif // STUDENT_H
