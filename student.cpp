/**
* @file student.cpp
* @author Léo B.
* @version 1.0.1
* @date 17 juin 2021
* 
*/
#include "student.h"

Student::Student(std::string name, bool present): m_name(name), m_present(present)
{

}
/**
 * Retourner le nom de l'étudiant
*/
std::string Student::name() const
{
    return m_name;
}

/**
 * Initialisation du nom de l'etudiant
*/
void Student::setName(const std::string &name)
{
    m_name = name;
}

/**
 * Relever la présence
*/
bool Student::present() const
{
    return m_present;
}

/**
 * Faire l' appel
*/
void Student::setPresent(bool present)
{
    m_present = present;
}

/**
 * Donner les info concernant 
*/
std::string Student::print() const
{
    std::string out = name() + " " + (present()?"true":"false");
    return out;
}

int Student::nb_presents(const std::vector<Student> &students)
{
    int n = 0;
    for(int i=0;i<students.size();i++)
        if(students.at(i).present())
            n++;
    return n;
}
